$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

window.Vue = require('vue');

Vue.component('application', require('./application.vue').default);

const app = new Vue({
    el: '#app'
});
